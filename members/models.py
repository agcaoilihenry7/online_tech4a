from django.db import models

class FormData(models.Model):
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    email = models.EmailField(default="your@email.com")
    gender = models.CharField(max_length=10, choices=[("male", "Male"), ("female", "Female")])
    terms = models.BooleanField(default=False)
    status = models.CharField(max_length=10, default="pending", choices=[("pending", "Pending"), ("active", "Active"), ("disabled", "Disabled")])
