from django.shortcuts import render
from .models import FormData

def form_view(request):
    if request.method == "POST":
        # Process the form data here
        username = request.POST.get("username")
        password = request.POST.get("p")  # Change 'p' to 'password'
        email = request.POST.get("email")
        gender = request.POST.get("gender")
        terms = request.POST.get("terms")

        # Convert the "terms" value to a boolean
        terms = True if terms == 'true' else False

        # Create a FormData instance and save it
        form_data = FormData(username=username, password=password, email=email, gender=gender, terms=terms)
        form_data.save()
    else:
        # Render the template with an empty form
        return render(request, 'form_template.html')

    return render(request, 'form_template.html')




def show_database(request):
    database_data = FormData.objects.all()
    return render(request, 'memdb.html', {'database_data': database_data})
