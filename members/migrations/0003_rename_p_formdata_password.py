# Generated by Django 4.2.6 on 2023-10-29 11:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0002_formdata_delete_userprofile'),
    ]

    operations = [
        migrations.RenameField(
            model_name='formdata',
            old_name='p',
            new_name='password',
        ),
    ]
