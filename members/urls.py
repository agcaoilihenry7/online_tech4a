from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_view, name='form_view'),
    path('show-database/', views.show_database, name='show_database'),  # New URL pattern
]
